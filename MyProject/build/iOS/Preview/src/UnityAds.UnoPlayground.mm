// This file was generated based on '/Users/Mau/Trabajos/iOS Apps/CodeExample/UnityAds/UnoPlayground.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#include <Fuse.Scripting.Context.h>
#include <Fuse.Scripting.NativeCallback.h>
#include <Fuse.Scripting.NativeFunction.h>
#include <Fuse.Scripting.NativeMember.h>
#include <UnityAds.UnoPlayground.h>
#include <UnityAdsManager.h>
#include <Uno.Bool.h>
#include <Uno.Object.h>
#include <Uno.String.h>
#include <Uno.UX.Resource.h>
#include <uObjC.Foreign.h>
static uString* STRINGS[4];
static uType* TYPES[1];

namespace g{
namespace UnityAds{

// public sealed class UnoPlayground :26
// {
static void UnoPlayground_build(uType* type)
{
    ::STRINGS[0] = uString::Const("UnoPlayground");
    ::STRINGS[1] = uString::Const("initAds");
    ::STRINGS[2] = uString::Const("isReady");
    ::STRINGS[3] = uString::Const("showAds");
    ::TYPES[0] = ::g::Fuse::Scripting::NativeCallback_typeof();
    type->SetInterfaces(
        ::g::Uno::IDisposable_typeof(), offsetof(::g::Fuse::Scripting::NativeModule_type, interface0),
        ::g::Fuse::Scripting::IModuleProvider_typeof(), offsetof(::g::Fuse::Scripting::NativeModule_type, interface1));
    type->SetFields(5,
        UnoPlayground_typeof(), (uintptr_t)&::g::UnityAds::UnoPlayground::_instance_, uFieldFlagsStatic);
    type->Reflection.SetFunctions(1,
        new uFunction(".ctor", NULL, (void*)UnoPlayground__New2_fn, 0, true, UnoPlayground_typeof(), 0));
}

::g::Fuse::Scripting::NativeModule_type* UnoPlayground_typeof()
{
    static uSStrong< ::g::Fuse::Scripting::NativeModule_type*> type;
    if (type != NULL) return type;

    uTypeOptions options;
    options.BaseDefinition = ::g::Fuse::Scripting::NativeModule_typeof();
    options.FieldCount = 6;
    options.InterfaceCount = 2;
    options.ObjectSize = sizeof(UnoPlayground);
    options.TypeSize = sizeof(::g::Fuse::Scripting::NativeModule_type);
    type = (::g::Fuse::Scripting::NativeModule_type*)uClassType::New("UnityAds.UnoPlayground", options);
    type->fp_build_ = UnoPlayground_build;
    type->fp_ctor_ = (void*)UnoPlayground__New2_fn;
    type->interface1.fp_GetModule = (void(*)(uObject*, ::g::Fuse::Scripting::Module**))::g::Fuse::Scripting::NativeModule__FuseScriptingIModuleProviderGetModule_fn;
    type->interface0.fp_Dispose = (void(*)(uObject*))::g::Fuse::Scripting::Module__Dispose_fn;
    return type;
}

// public UnoPlayground() :31
void UnoPlayground__ctor_2_fn(UnoPlayground* __this)
{
    __this->ctor_2();
}

// private static extern void initAds() :64
void UnoPlayground__initAds_fn()
{
    UnoPlayground::initAds();
}

// private object initAds(Fuse.Scripting.Context c, object[] args) :42
void UnoPlayground__initAds1_fn(UnoPlayground* __this, ::g::Fuse::Scripting::Context* c, uArray* args, uObject** __retval)
{
    *__retval = __this->initAds1(c, args);
}

// private static extern bool isReady() :71
void UnoPlayground__isReady_fn(bool* __retval)
{
    *__retval = UnoPlayground::isReady();
}

// private object isReady(Fuse.Scripting.Context c, object[] args) :49
void UnoPlayground__isReady1_fn(UnoPlayground* __this, ::g::Fuse::Scripting::Context* c, uArray* args, uObject** __retval)
{
    *__retval = __this->isReady1(c, args);
}

// public UnoPlayground New() :31
void UnoPlayground__New2_fn(UnoPlayground** __retval)
{
    *__retval = UnoPlayground::New2();
}

// private static extern void showAds() :79
void UnoPlayground__showAds_fn()
{
    UnoPlayground::showAds();
}

// private object showAds(Fuse.Scripting.Context c, object[] args) :56
void UnoPlayground__showAds1_fn(UnoPlayground* __this, ::g::Fuse::Scripting::Context* c, uArray* args, uObject** __retval)
{
    *__retval = __this->showAds1(c, args);
}

uSStrong<UnoPlayground*> UnoPlayground::_instance_;

// public UnoPlayground() [instance] :31
void UnoPlayground::ctor_2()
{
    uStackFrame __("UnityAds.UnoPlayground", ".ctor()");
    ctor_1();

    if (UnoPlayground::_instance_ != NULL)
        return;

    UnoPlayground::_instance_ = this;
    ::g::Uno::UX::Resource::SetGlobalKey(UnoPlayground::_instance_, ::STRINGS[0/*"UnoPlayground"*/]);
    AddMember(::g::Fuse::Scripting::NativeFunction::New1(::STRINGS[1/*"initAds"*/], uDelegate::New(::TYPES[0/*Fuse.Scripting.NativeCallback*/], (void*)UnoPlayground__initAds1_fn, this)));
    AddMember(::g::Fuse::Scripting::NativeFunction::New1(::STRINGS[2/*"isReady"*/], uDelegate::New(::TYPES[0/*Fuse.Scripting.NativeCallback*/], (void*)UnoPlayground__isReady1_fn, this)));
    AddMember(::g::Fuse::Scripting::NativeFunction::New1(::STRINGS[3/*"showAds"*/], uDelegate::New(::TYPES[0/*Fuse.Scripting.NativeCallback*/], (void*)UnoPlayground__showAds1_fn, this)));
}

// private object initAds(Fuse.Scripting.Context c, object[] args) [instance] :42
uObject* UnoPlayground::initAds1(::g::Fuse::Scripting::Context* c, uArray* args)
{
    uStackFrame __("UnityAds.UnoPlayground", "initAds(Fuse.Scripting.Context,object[])");
    UnoPlayground::initAds();
    return NULL;
}

// private object isReady(Fuse.Scripting.Context c, object[] args) [instance] :49
uObject* UnoPlayground::isReady1(::g::Fuse::Scripting::Context* c, uArray* args)
{
    uStackFrame __("UnityAds.UnoPlayground", "isReady(Fuse.Scripting.Context,object[])");
    bool _b = UnoPlayground::isReady();
    return uBox(::g::Uno::Bool_typeof(), _b);
}

// private object showAds(Fuse.Scripting.Context c, object[] args) [instance] :56
uObject* UnoPlayground::showAds1(::g::Fuse::Scripting::Context* c, uArray* args)
{
    uStackFrame __("UnityAds.UnoPlayground", "showAds(Fuse.Scripting.Context,object[])");
    UnoPlayground::showAds();
    return NULL;
}

// private static extern void initAds() [static] :64
void UnoPlayground::initAds()
{
    uStackFrame __("UnityAds.UnoPlayground", "initAds()");
    @autoreleasepool
    {
        NSLog(@"initAds===");
        			[UnityAdsManager init];
    }
    
}

// private static extern bool isReady() [static] :71
bool UnoPlayground::isReady()
{
    uStackFrame __("UnityAds.UnoPlayground", "isReady()");
    @autoreleasepool
    {
        BOOL _b = [UnityAdsManager isReady];
        			//NSLog(@"isReady===%d", _b);
        			return _b;
    }
    
}

// public UnoPlayground New() [static] :31
UnoPlayground* UnoPlayground::New2()
{
    UnoPlayground* obj1 = (UnoPlayground*)uNew(UnoPlayground_typeof());
    obj1->ctor_2();
    return obj1;
}

// private static extern void showAds() [static] :79
void UnoPlayground::showAds()
{
    uStackFrame __("UnityAds.UnoPlayground", "showAds()");
    @autoreleasepool
    {
        NSLog(@"showAds===");
        			[UnityAdsManager showAds];
    }
    
}
// }

}} // ::g::UnityAds
