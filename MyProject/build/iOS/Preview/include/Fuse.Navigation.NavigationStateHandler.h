// This file was generated based on /usr/local/share/uno/Packages/Fuse.Navigation/0.40.8/$.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Fuse{
namespace Navigation{

// internal delegate void NavigationStateHandler(object page, Fuse.Navigation.NavigationStateArgs args) :1066
uDelegateType* NavigationStateHandler_typeof();

}}} // ::g::Fuse::Navigation
