// This file was generated based on /usr/local/share/uno/Packages/UnoCore/0.40.2/source/uno/content/fonts/$.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <uImage/FontFace.h>
#include <Uno.Object.h>
namespace uImage { class FontFace; }

namespace g{
namespace Uno{
namespace Content{
namespace Fonts{

// internal extern struct CppFontFaceHandle :188
// {
uStructType* CppFontFaceHandle_typeof();

struct CppFontFaceHandle
{
};
// }

}}}} // ::g::Uno::Content::Fonts
