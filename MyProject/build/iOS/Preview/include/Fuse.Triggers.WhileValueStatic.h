// This file was generated based on /usr/local/share/uno/Packages/Fuse.Triggers/0.40.8/$.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.h>
namespace g{namespace Fuse{namespace Triggers{struct WhileValueStatic;}}}

namespace g{
namespace Fuse{
namespace Triggers{

// internal static class WhileValueStatic :3723
// {
uClassType* WhileValueStatic_typeof();

struct WhileValueStatic : uObject
{
    static bool _deprecatedNote_;
    static bool& _deprecatedNote() { return _deprecatedNote_; }
};
// }

}}} // ::g::Fuse::Triggers
