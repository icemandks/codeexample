// This file was generated based on /usr/local/share/uno/Packages/FuseCore/0.40.8/input/$.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Fuse{
namespace Input{

// public delegate void PointerEnteredHandler(object sender, Fuse.Input.PointerEnteredArgs args) :866
uDelegateType* PointerEnteredHandler_typeof();

}}} // ::g::Fuse::Input
