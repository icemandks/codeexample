// This file was generated based on /usr/local/share/uno/Packages/Fuse.Reactive/0.40.8/.uno/package.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.h>
namespace g{namespace Uno{namespace IO{struct BundleFile;}}}
namespace g{struct FuseReactive_bundle;}

namespace g{

// public static generated class FuseReactive_bundle :0
// {
uClassType* FuseReactive_bundle_typeof();

struct FuseReactive_bundle : uObject
{
    static uSStrong< ::g::Uno::IO::BundleFile*> es6promise329105ac_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& es6promise329105ac() { return FuseReactive_bundle_typeof()->Init(), es6promise329105ac_; }
    static uSStrong< ::g::Uno::IO::BundleFile*> Observablee36318f0_;
    static uSStrong< ::g::Uno::IO::BundleFile*>& Observablee36318f0() { return FuseReactive_bundle_typeof()->Init(), Observablee36318f0_; }
};
// }

} // ::g
