// This file was generated based on '/Users/Mau/Trabajos/iOS Apps/CodeExample/UnityAds/UnoPlayground.uno'.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Scripting.IModuleProvider.h>
#include <Fuse.Scripting.NativeModule.h>
#include <Uno.IDisposable.h>
namespace g{namespace Fuse{namespace Scripting{struct Context;}}}
namespace g{namespace UnityAds{struct UnoPlayground;}}

namespace g{
namespace UnityAds{

// public sealed class UnoPlayground :26
// {
::g::Fuse::Scripting::NativeModule_type* UnoPlayground_typeof();
void UnoPlayground__ctor_2_fn(UnoPlayground* __this);
void UnoPlayground__initAds_fn();
void UnoPlayground__initAds1_fn(UnoPlayground* __this, ::g::Fuse::Scripting::Context* c, uArray* args, uObject** __retval);
void UnoPlayground__isReady_fn(bool* __retval);
void UnoPlayground__isReady1_fn(UnoPlayground* __this, ::g::Fuse::Scripting::Context* c, uArray* args, uObject** __retval);
void UnoPlayground__New2_fn(UnoPlayground** __retval);
void UnoPlayground__showAds_fn();
void UnoPlayground__showAds1_fn(UnoPlayground* __this, ::g::Fuse::Scripting::Context* c, uArray* args, uObject** __retval);

struct UnoPlayground : ::g::Fuse::Scripting::NativeModule
{
    static uSStrong<UnoPlayground*> _instance_;
    static uSStrong<UnoPlayground*>& _instance() { return _instance_; }

    void ctor_2();
    uObject* initAds1(::g::Fuse::Scripting::Context* c, uArray* args);
    uObject* isReady1(::g::Fuse::Scripting::Context* c, uArray* args);
    uObject* showAds1(::g::Fuse::Scripting::Context* c, uArray* args);
    static void initAds();
    static bool isReady();
    static UnoPlayground* New2();
    static void showAds();
};
// }

}} // ::g::UnityAds
