// This file was generated based on /usr/local/share/uno/Packages/FuseCore/0.40.8/scripting/$.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Fuse{
namespace Scripting{

// public delegate object Callback(object[] args) :2463
uDelegateType* Callback_typeof();

}}} // ::g::Fuse::Scripting
