// This file was generated based on /usr/local/share/uno/Packages/FuseCore/0.40.8/$.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Fuse{
namespace Animations{

// public delegate float EasingFunction(float f) :1709
uDelegateType* EasingFunction_typeof();

}}} // ::g::Fuse::Animations
