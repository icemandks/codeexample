// This file was generated based on /usr/local/share/uno/Packages/Fuse.Gestures/0.40.8/internal/$.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Fuse{
namespace Gestures{
namespace Internal{

// internal delegate void TwoFingerRotateHandler(float angle) :1235
uDelegateType* TwoFingerRotateHandler_typeof();

}}}} // ::g::Fuse::Gestures::Internal
