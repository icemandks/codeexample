// This file was generated based on /usr/local/share/uno/Packages/UnoCore/0.40.2/source/uno/$.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Uno{

// public delegate bool Predicate<T>(T arg) :5250
uDelegateType* Predicate_typeof();

}} // ::g::Uno
