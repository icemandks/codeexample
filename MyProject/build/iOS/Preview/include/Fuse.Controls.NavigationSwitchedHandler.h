// This file was generated based on /usr/local/share/uno/Packages/Fuse.Controls.Navigation/0.40.8/$.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Fuse{
namespace Controls{

// internal delegate void NavigationSwitchedHandler(object sender, Fuse.Controls.NavigatorSwitchedArgs args) :794
uDelegateType* NavigationSwitchedHandler_typeof();

}}} // ::g::Fuse::Controls
