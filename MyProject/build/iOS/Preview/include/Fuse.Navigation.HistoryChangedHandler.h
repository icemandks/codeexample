// This file was generated based on /usr/local/share/uno/Packages/Fuse.Navigation/0.40.8/$.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Fuse{
namespace Navigation{

// public delegate void HistoryChangedHandler(object sender) :610
uDelegateType* HistoryChangedHandler_typeof();

}}} // ::g::Fuse::Navigation
