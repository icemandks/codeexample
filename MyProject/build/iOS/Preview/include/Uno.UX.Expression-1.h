// This file was generated based on /usr/local/share/uno/Packages/Fuse.Triggers/0.40.8/actions/$.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Uno{
namespace UX{

// public delegate T Expression<T>() :611
uDelegateType* Expression_typeof();

}}} // ::g::Uno::UX
