// This file was generated based on /usr/local/share/uno/Packages/Fuse.Controls.ScrollView/0.40.8/$.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Fuse{
namespace Controls{

// public delegate void ScrollPositionChangedHandler(object sender, Fuse.Controls.ScrollPositionChangedArgs args) :963
uDelegateType* ScrollPositionChangedHandler_typeof();

}}} // ::g::Fuse::Controls
