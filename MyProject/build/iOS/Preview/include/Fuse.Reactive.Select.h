// This file was generated based on /usr/local/share/uno/Packages/Fuse.Reactive/0.40.8/$.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Fuse.Animations.IPlayerFeedback.h>
#include <Fuse.Binding.h>
#include <Fuse.Reactive.With.h>
#include <Fuse.Scripting.IScriptObject.h>
#include <Uno.Collections.ICollection-1.h>
#include <Uno.Collections.IEnumerable-1.h>
#include <Uno.Collections.IList-1.h>
namespace g{namespace Fuse{namespace Reactive{struct Select;}}}

namespace g{
namespace Fuse{
namespace Reactive{

// public sealed class Select :2493
// {
::g::Fuse::Triggers::Trigger_type* Select_typeof();
void Select__ctor_5_fn(Select* __this);
void Select__New3_fn(Select** __retval);

struct Select : ::g::Fuse::Reactive::With
{
    void ctor_5();
    static Select* New3();
};
// }

}}} // ::g::Fuse::Reactive
