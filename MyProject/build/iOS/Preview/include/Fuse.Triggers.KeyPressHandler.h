// This file was generated based on /usr/local/share/uno/Packages/Fuse.Triggers/0.40.8/$.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Delegate.h>

namespace g{
namespace Fuse{
namespace Triggers{

// public delegate void KeyPressHandler(object sender, Fuse.Input.KeyEventArgs args) :756
uDelegateType* KeyPressHandler_typeof();

}}} // ::g::Fuse::Triggers
