// This file was generated based on /usr/local/share/uno/Packages/Fuse.iOS/0.40.8/$.uno.
// WARNING: Changes might be lost if you edit this file directly.

#pragma once
#include <Uno.Object.h>

namespace g{
namespace Fuse{
namespace iOS{
namespace Bindings{

// internal extern struct CGContextRef :737
// {
uStructType* CGContextRef_typeof();

struct CGContextRef
{
    void* _dummy;
};
// }

}}}} // ::g::Fuse::iOS::Bindings
